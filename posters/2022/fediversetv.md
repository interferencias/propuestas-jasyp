---
layout: 2022/post
section: proposals
category: posters
author: FediverseTV
title: Hola, somos FediverseTV
---

# Hola, somos FediverseTV

FediverseTV (FTV) es una comunidad construida alrededor de Peertube, que es un software de servidor que facilita la construcción de plataformas para la publicación y emisión de vídeos y lo hace de manera federada y descentralizada. Permite subir vídeos, hacer directos y que estos se compartan  automáticamente  con  comunidades que consideramos amigas, lo cual dificulta la censura y mejora la disponibilidad de estos.

Instancia es cada instalación de este código en un servidor y con un dominio propio, decimos que es una red   federada porque cada una de estas instancias puede federar con otras, es decir pueden mostrar sus contenidos a otras y estas pueden mostrar los suyos a la primera a su vez, todas las instancias son iguales entre si y cada una pone sus reglas en su espacio.

La comunidad de FTV nació con el fin de albergar contenido afín a nuestra ideología anticapitalista, antirracista y transfeminista.  Por ello, ya que nos relacionamos con otras comunidades simpatizantes, tenemos mucho cuidado con cuales si o no permitimos estas relaciones, ya que queremos mantener nuestra ideología. No es una instancia dedicada a un contenido en concreto, se puede subir de cualquier tipo, siempre y cuando no incumpla el manifiesto de convivencia.

En definitiva y abreviando… FTV es una plataforma para subir vídeos y hacer directos que federa con otras instancias de Peertube  y otros programas, que en su conjunto se conocen como Fediverso.

## Proyecto:

-   Enlace al póster en Wikimedia Commons: <https://commons.wikimedia.org/wiki/File:Cartel_FediverseTV.png>
-   Web del proyecto: <https://fediverse.tv>

## Autor/a:

-   Nombre: FediverseTV
-   Bio: FediverseTV (FTV) es una comunidad construida alrededor de Peertube, que es un software de servidor que facilita la construcción de plataformas para la publicación y emisión de vídeos y lo hace de manera federada y descentralizada. Permite subir vídeos, hacer directos y que estos se compartan  automáticamente  con  comunidades que consideramos amigas, lo cual dificulta la censura y mejora la disponibilidad de estos.

### Info personal:

-   Web personal: <https://blog.fediverse.tv> / <hola@fediverse.tv>
-   Mastodon (u otras redes sociales libres): <https://mastodon.art/@fediversetv> / <https://matrix.to/#/#peertubevideos:matrix.org>
-   Twitter:
-   GitLab (o cualquier sitio de código colaborativo) o portfolio:

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://jasyp.interferencias.tech/conducta>) durante mi participación en las jornadas
