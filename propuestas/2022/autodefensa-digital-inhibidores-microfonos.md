---
layout: 2022/post
section: proposals
category: proposals
author: Jorge Cuadrado Sáez (Coke)
title: Autodefensa digital, inhibidores de micrófonos
---

# Autodefensa digital: inhibidores de micrófonos

El mundo gira alrededor de la tecnología, es inevitable el estar relacionado con ella. El teléfono móvil se ha convertido en una necesidad, ya no sólo en el día a día, lo necesitamos para interactuar con las administraciones públicas o las empresas que proveen servicios esenciales. La domótica se conecta a asistentes de voz. En breve los wearables estarán cosidos a nuestra ropa... Cuantos más sensores a nuestro alrededor, menos certeza de nuestra privacidad. ¿Son las empresas éticas al tratar nuestros datos? No podemos asegurarlo, el software es difícilmente auditable, más cuando está alojado en entornos cloud. ¿Cómo podemos seguirle el ritmo a la tecnología sin perder nuestra privacidad? No se puede, pero al menos podemos ponerselo más difícil a las empresas que no gestionan nuestros datos correctamente. Contra el espionaje... autodefensa y contra los micrófonos... inhibidores. ¿Te gustaría saber como construir un inhibidor de micrófonos portátil que se pueda llevar en la muñeca? Hoy hablamos de eso.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga (40 minutos)
-   Descripción: Una charla distendida para cualquier persona. Intentaré hacer una demo en vivo sobre como se inhibe un micrófono y contaré el proceso de construcción de un inhibidor DIY. No quiero entrar en mucho tecnicismo porque prefiero que la charla llegue a cualquier persona. Liberaré todo el material al terminar la charla. No prometo seriedad en el escenario, preparad la NERF si queréis sacarme de ahí...
-   Web del proyecto:
-   Público objetivo: Cualquier persona que este preocupada por su privacidad y quiera tomar el control de la información que comparte o se filtra sobre él. En concreto, aquellas que tengan especial preocupación por que sus conversaciones puedan estar siendo grabadas con un micrófono.

## Ponente:

-   Nombre: Jorge Cuadrado Sáez (Coke)
-   Bio: Creo firmemente en que la divulgación y la privacidad son dos componentes necesarios e imprescindibles en una sociedad que avanza tecnológicamente de forma ética y sana para las personas. En los últimos años ya he hablado de decenas de tecnologías que favorecen o perjudican a nuestra privacidad, y es que, muy pocas organizaciones enfocan su modelo de negocio en productos que mantengan o protejan la privacidad de sus usuarios. Mis charlas tratan sobre eso, tecnología, pero me gusta hablar sobre la que nos afecta directamente, proponiendo alternativas y mejoras. Siempre me gusta mezclar estos temas con mis hobbies: privacidad, electronica y DIY. Para ver más datos aburridos sobre mí, o ver otras charlas que he dado, tienes mis redes sociales o puedes bichear mi página web, también puedes contactarme para charlar un rato sobre cualquier cosa.

### Info personal:

-   Web personal: <https://www.croke.es>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/@coke727>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/jorcuad>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://jasyp.interferencias.tech/conducta>) durante mi participación en las jornadas
