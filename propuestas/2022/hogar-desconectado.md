---
layout: 2022/post
section: proposals
category: proposals
author: Manuel Martín (draxus)
title: Hogar (Des)Conectado
---

# Hogar (Des)Conectado

Cada vez estamos viendo más y más aparatos "inteligentes" se cuelan en nuestros hogares. Ya sean bombillas, enchufes o sensores de temperatura, lo que tienen casi todos en común es la necesidad de estar conectados a un hub central específico de cada marca así como una aplicación móvil para configurarlos. Además, una de las características más anunciada es su integración con Alexa o Google Assistant. Todo esto implica por lo general que nuestros aparatos van a estar conectados permanentemente a internet y enviando nuestros datos a una o mútiples compañías. ¿Existen alternativas para disfrutar de esta tecnología en casa sin "hipotecarnos" con los dispositivos de una compañía concreta y para tener el control de nuestros datos? En esta charla contaré mi experiencia domotizando mi casa manteniendo la privacidad. Hashtags: #zigbee #domotica #sensores #raspberrypi #privacidad

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta (20 minutos)
-   Descripción: En la charla contaré mi experiencia configurando la Raspberry Pi con el hub ConBee 2 para conectar los dispositivos Zigbee de diferentes marcas (a priori solo compatibles con sus respectivos hubs). Explicaré también cómo usar servicios libres de automatización y control de dispositivos como HomeAssistant y Homebridge.
-   Web del proyecto:
-   Público objetivo: A cualquier persona interesada en preservar la privacidad digital de su hogar

## Ponente:

-   Nombre: Manuel Martín (draxus)
-   Bio: Soy un entusiasta de la tecnología y el software libre. Actualmente trabajo como investigador y desarrollador de modelos de machine learning. Me gusta cacharrear con la Raspberry Pi.

### Info personal:

-   Web personal: <https://www.draxus.org/>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/draxus>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/draxus>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://jasyp.interferencias.tech/conducta>) durante mi participación en las jornadas
