---
layout: 2022/post
section: proposals
category: proposals
author: Juan Velasco
title: Actores de amenaza fantásticos y dónde encontrarlos
---

# Actores de amenaza fantásticos y dónde encontrarlos

A raíz del conflicto internacional entre Rusia y Ucrania, los analistas de ciberinteligencia están muy interesados en analizar todas las técnicas, tácticas y procedimientos de los principales actores de amenaza (APTs, grupos descentralizados, actores individuales), sus motivaciones y sus objetivos.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta (20 minutos)
-   Descripción: El objetivo de esta charla es dar una introducción al mundo de la ciberinteligencia desde el punto de vista de Threat Intelligence, identificando los principales actores de amenaza estatales y sus capacidades, los grupos descentralizados que operan en foros de cibercrimen y sus acciones.
-   Web del proyecto:
-   Público objetivo: •	Ciberinteligencia
•	Threat Intelligence
•	Equipos de respuesta (Blue Team)
•	Equipos ofensivos (Red Team, veremos también CVEs más explotados)
•	Amantes de la ciberseguridad y ciberinteligencia
•	Estudiantes de grados técnicos como informática, telecomunicaciones o matemáticas (ETSIIT)


## Ponente:

-   Nombre: Juan Velasco
-   Bio: Hola a todos! Que alegría volver a ver que se puede organizar unas jornadas JASYP presenciales. Soy Juan Velasco, uno de los primeros "miembros" del grupo y ponente en la primera de las jornadas JASYP en temas de ciberguerra. He visto el call for papers y no he podido evitar querer formar parte de ello. Desde que terminé el grado en informática y matemáticas en Granada he continuado en el ámbito profesional centrado en investigación de amenazas y actores estatales. Quedo a vuestra disposición.

### Info personal:

-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   GitLab (o cualquier sitio de código colaborativo) o portfolio:

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://jasyp.interferencias.tech/conducta>) durante mi participación en las jornadas
