---
layout: 2022/post
section: proposals
category: proposals
author: Jorge Maldonado Ventura
title: La cultura es libre, una historia de la resistencia antipropiedad
---

# La cultura es libre: una historia de la resistencia antipropiedad

Presentación del libro "La cultura es libre: una historia de la resistencia antipropiedad", con una pequeña demostración al final mostrando cómo generar un libro en varios formatos (PDF, HTML, EPUB). Mi intención es que la gente aprenda más sobre las batallas que ha habido a lo largo de la historia contra el cierre de la cultura, que han dado lugar al software libre y la cultura libre.

## Detalles de la propuesta:

-   Tipo de propuesta: Otra
-   Descripción: El libro se genera a partir de archivos markdown mediante Pandoc. El libro tiene los siguientes capítulos:
  - cultura oral
  - cultura impresa
  - cultura propietaria
  - cultura recombinante
  - cultura libre
  - cultura colectiva

Hablaré un poco de cada uno, pero al que dedicaré más tiempo será el de la cultura libre, que es también el capítulo más largo del libro. El libro es una traducción de _A Cultura é Livre: Uma história da resistência antipropriedade_ de Leonardo Foletto, autor de baixacultura.org
-   Web del proyecto: <https://freakspot.net/libro/la-cultura-es-libre/>
-   Público objetivo: Personas que tengan curiosidad sobre el software libre y la cultura libre. A quienes les gustaría aprender cómo sacar un libro en varios formatos (EPUB, PDF, HTML).
Quienes se interesan por la resistencia a las restricciones a la cultura, abordada desde la política, la sociología, la antropología y la historia.

## Ponente:

-   Nombre: Jorge Maldonado Ventura
-   Bio: Soy un traductor y programador que lleva años contribuyendo a diversos proyectos de software libre y de cultura libre. He participado y dado charlas en eventos similares como el Hackmeeting, PyConEs, Fosdem, GNU Hackers' Meeting, etc.

### Info personal:

-   Web personal: <https://freakspot.net/>
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://notabug.org/jorgesumle>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://jasyp.interferencias.tech/conducta>) durante mi participación en las jornadas
