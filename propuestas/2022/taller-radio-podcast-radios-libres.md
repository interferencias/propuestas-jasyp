---
layout: 2022/post
section: proposals
category: proposals
author: Post Nau Crew
title: PostNau taller de radio y podcast desde las radios libres
---

# PostNau taller de radio y podcast desde las radios libres

Ofreceremos un breve taller en el que iniciaremos a las herramientas básicas para poder hacer un podcast o programa de radio libre.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller (30-60 minutos)
-   Descripción: En el taller pretendemos dar algunas pinceladas y herramientas sobre:

-Escaleta
-Guion
-Locución
-Técnica
-Realización y coordinación

La idea es grabar algunas producciones durante el proceso del taller.
-   Web del proyecto: <https://www.elsaltodiario.com/post-apocalipsis-nau>
-   Público objetivo: Creemos que este taller puede resultar positivo para cualquier persona que esté interesada en realizar comunicación sobre el área en el que sea experta y en aprender dinámicas de trabajo en grupo para sacar adelante proyectos.

## Ponente:

-   Nombre: Post Nau Crew
-   Bio: Somos un equipo de varias personas que realizamos un programa de radio sobre tecnología, música electrónica y memes desde una perspectiva crítica. Formamos parte de Radio Vallekas, radio libre de Madrid. Publicamos nuestro podcast en elsaltodiario.

### Info personal:

-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/@PostNau>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio:

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://jasyp.interferencias.tech/conducta>) durante mi participación en las jornadas
