---
layout: 2022/post
section: proposals
category: proposals
author: Paula
title: Cómo reportar eventos de forma segura
---

# Cómo reportar eventos de forma segura

El taller es una introducción a qué tipo de herramientas y cómo podemos cubrir un evento, manifestación o entrevista de forma que cuidemos la privacidad y la seguridad de las personas que participen. 

## Detalles de la propuesta:

-   Tipo de propuesta: Taller (30-60 minutos)
-   Descripción: El taller pretende durar unos 45 minutos. Para llevarlo a cabo revisaremos qué amenazas digitales y de privacidad encontramos en varios tipos de eventos y posteriormente trataremos cómo solventarlos, por ejemplo: metadatos en imágenes,  personas en fotos, documentación en tiempo real sin usar herramientas privativas, redes resilientes, etc.

Al final del taller también se responderán a preguntas específicas sobre situaciones concretas si es que las hay. Para aprovechar al máximo el taller se requiere que las personas participantes lleven un portátil con Linux.
-   Web del proyecto:
-   Público objetivo: Cualquier persona que tenga interés en ayudar a cubrir eventos o cuidarse de la seguridad y la privacidad en redes.

## Ponente:

-   Nombre: Paula
-   Bio: CIberinteligencia en Sentinel One. Profesora de pentesting y hardware hacking en FP de ciberseguridad en Los Salesianos Atocha (Madrid). Presidenta cofundadora de Interferencias y colaboradora en Post Apocalipsis Nau.

### Info personal:

-   Web personal: <https://terceranexus6.gitlab.io/website/>
-   Mastodon (u otras redes sociales libres): <https://cybre.space/@terceranexus6/>
-   Twitter: <https://twitter.com/@Terceranexus6>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://gitlab.com/terceranexus6>

## Comentarios



## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://jasyp.interferencias.tech/conducta>) durante mi participación en las jornadas

