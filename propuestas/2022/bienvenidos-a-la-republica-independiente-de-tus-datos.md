---
layout: 2022/post
section: proposals
category: proposals
author: JJ Merelo
title: Bienvenidos a la república independiente de tus datos
---

# Bienvenidos a la república independiente de tus datos

Los datos personales son el combustible de la economía digital moderna, donde "un app gratis" viene a significar "vamos a agarrar tus datos y venderlos por todos sitios, incluso datos que no pensabas que pudiéramos tener". Hay muchas razones para que se haya llegado a esta situación, pero una de ellas es que no existen herramientas personales que permitan a alguien gestionar, visualizar y autorizar (o no) acceso a diferentes aspectos (o facetas) de sus datos personales.

En polypoly nuestra misión es crear esa plataforma y nuestra visión es que, al posibilitar la soberanía de datos del usuario regular, seremos capaces de crear una economía de datos equitativa donde el usuario pueda gestionar qué datos personales tienen las compañías, qué pueden hacer con ellos, puedan gestionar también esos datos de forma sencilla (permitiendo las provisiones que hay en la GDPR de borrado y modificación, por ejemplo) y eventualmente se pueda beneficiar económicamente de cualquier aspecto de esos datos que los haga únicos, siempre bajo su control.

Para conseguir esto, hemos creato una plataforma de gestión de datos personal llamada polyPod, apoyada por una enciclopedia de datos sobre compañías (y qué hacen estas compañías con los datos que recolectan) llamada la polyPedia. El polyPod es software libre, y consiste actualmente en dos aplicaciones para iOS y Android, respectivamente, y que también está disponible en https://github.com/polypoly-eu/polyPod.

En esta charla, presentaremos los conceptos generales de soberanía de datos en los que se basa la compañía (y también la cooperativa y la fundación) y sus productos, así como una visión general de las herramientas que contiene el polyPod y que estamos usando dentro del mismo para implementar esa visión de soberanía de datos personales, la república independiente de los datos personales.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga (40 minutos)
-   Descripción: Se trata de presentar por un lado conceptos generales de soberanía de datos, y luego, a un nivel divulgativo, las capacidades técnicas del producto y cómo se pueden tratar los datos personales que te dejan descargarte sitios como Facebook (y, posiblemente, en el momento en que se dé la charla, Google) para entender mejor lo que las grandes empresas saben de ti y cómo evitar que sepan tanto, o entender qué hacen con esa información.
-   Web del proyecto: <https://polypoly.com>
-   Público objetivo: En general, cualquier público que tenga unos conocimientos mínimos de programación.

## Ponente:

-   Nombre: JJ Merelo
-   Bio: Participante en todas las ediciones de las JASYP, amigo de Raku, y también ingeniero software senior en polypoly. Ah, y profe.

### Info personal:

-   Web personal: <https://jj.github.io>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/jjmerelo>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://gitlab.com/JJ5>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://jasyp.interferencias.tech/conducta>) durante mi participación en las jornadas
