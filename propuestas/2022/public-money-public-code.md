---
layout: 2022/post
section: proposals
category: proposals
author: Alexander Sander
title: Public Money? Public Code! During Corona
---

# Public Money? Public Code! During Corona

We will take a look at the role that Free Software played in the last crisis and how it could help overcome future ones, by pointing to the principle of “Public Money? Public Code! and the role that governments, public bodies, administrations have in this.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga (40 minutos)
-   Descripción: The Corona crisis has made clear not only how much we rely on digital infrastructure, but also how much digital development we have missed in recent years. Administrations, and many employers as well, were not prepared for this. Some solutions were chosen to overcome this challenge without much consideration of the effects and consequences that these technologies can have. Data and privacy risks were not taken into consideration, nor the dependency on single vendors.
Nevertheless, some good practices have taken place, and Free Software has become more a more of a topic and more and more administration have understood the advantages it provides.
We will take a look at the role that Free Software played in the last crisis and how it could help overcome future ones, by pointing to the principle of “Public Money? Public Code! and the role that governments, public bodies, administrations have in this.
-   Web del proyecto: <https://publiccode.eu/>
-   Público objetivo: A cualquier persona interesada en privacidad, uso eficiente de recursos públicos, activistas y/o usuarios de Software Libre. Así como funcionarios públicos o personas trabajando en administraciones públicas.

## Ponente:

-   Nombre: Alexander Sander
-   Bio: Alexander has studied politics in Marburg and later has been an MEP Assistant in Brussels for three years and the General Manager of Digitale Gesellschaft e.V. in Berlin for four years. Furthermore, he is the founder of NoPNR!, a campaign against the retention of travel data. He is currently a Policy Consultant at the FSFE.

### Info personal:

-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/@lexelas>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio:

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://jasyp.interferencias.tech/conducta>) durante mi participación en las jornadas
