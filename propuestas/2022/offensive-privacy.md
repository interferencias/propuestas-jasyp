---
layout: 2022/post
section: proposals
category: proposals
author: Pepe Moreno
title: Offensive Privacy - methods & gadgets, long live OpSec
---

# Offensive Privacy: methods & gadgets, long live OpSec

Ponencia sobre técnicas, metodologias y herramientas enfocadas a privacidad, desde la perspectiva de un pentester o distintos perfiles ofensivos.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga (40 minutos)
-   Descripción: Ponencia sobre técnicas, metodologias y herramientas enfocadas a privacidad, desde la perspectiva de un pentester o distintos perfiles ofensivos dentro del mundo de la tecnologia que quiere desea realizar un trabajo de OpSec sobre su infrastructura, realizando una comparativa de estas tecnicas, entendiendolas en distintos contextos en los que su aplicacion tiene mas o menos sentido" La ponencia se enfoca para todo tipo de perfiles interesados en la privacidad. Tecnicos del mundo del hacking y la seguridad que puedan aplicar este tipo de tecnicas para evadir todo tipo de controles, medidad o simplemente, "no dejar rastro". Asi como activistas, periodistas, perfiles interesados en tener a su alcance distintas meodologias que no expongan o lo hagan de forna controla la informacion con la que tratan.
-   Web del proyecto: <https://bitupalicante.com>
-   Público objetivo: Publico técnico (no de gran nivel), interesados por el tema o aficionados de la seguridad informática.

## Ponente:

-   Nombre: Josep Moreno Zamorano aka JoMoZa
-   Bio: Administrador de la comunidad BitUp Alicante. Pentester en Wise Security. Colaboro con varias comunidades de hacking y privacidad en internet. Apasionado de la tecnología, la comunidad y cultura de internet. Un pedazo de friki como un monte vamos.

### Info personal:

-   Web personal: <https://loveisinthe.net/>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/@j0moz4> / <https://twitter.com/bitupalicante>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/jomoza>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://jasyp.interferencias.tech/conducta>) durante mi participación en las jornadas
