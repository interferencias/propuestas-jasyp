---
layout: 2022/post
section: proposals
category: proposals
author: Lina Ceballos
title: Taller ¿Dinero Público? ¡Código Público! "Contacta tus administraciones"
---

# Taller ¿Dinero Público? ¡Código Público! "Contacta tus administraciones"

En el marco de nuestra campaña ¿Dinero Público? ¡Código Público! queremos ofrecer este taller dirigido a voluntarios y a la sociedad civil para motivarlos a que se pongan en contacto directo con sus administraciones públicas para que exijan más uso de Software Libre en entes públicos. Si es dinero público, debería ser código público!

## Detalles de la propuesta:

-   Tipo de propuesta: Taller (30-60 minutos)
-   Descripción: Nuestra iniciativa "¿Dinero público? ¡Código Público!" tiene el propósito de que el Software financiado con dinero público y utilizado en la administración pública sea Software Libre. Tenemos una carta abierta, la cual puede ser firmada como individuo o como organización no gubernamental (ONG). Hasta ahora tenemos más de 30.000 personas y casi 200 ONGs apoyando nuestro objetivo. Y ya cinco entidades públicas, la sueca JobTech Development, la alemana "Samtgemeinde Elbmarch", la Junta General del Principado de Asturias, la ciudad española de Benigànim y la ciudad española de Barcelona firmaron nuestra carta abierta. Nos gustaría tener más unidades administrativas que apoyen nuestra causa. Pero para lograrlo necesitamos más participación comunitaria.
El objetivo de este taller es compartir con los participantes un enfoque más práctico de acercamiento a administraciones públicas, compartiendo las mejores prácticas y ayudándoles a construir mejores argumentos para concienciar a las administraciones de nuestra campaña ¿Dinero Público? ¡Código Público!, y compartir los beneficios del Software Libre. El fin de este taller, es llegar a más y más administraciones publicas, así como también, capacitar y empoderar población civil a que sea más activa dentro de sus propias comunidades.
-   Web del proyecto: <https://publiccode.eu/ >
-   Público objetivo: Voluntarios y simpatizantes de toda Europa que quieren ser activos para llegar a sus representantes locales y exigir más Software Libre.

## Ponente:

-   Nombre: Lina Ceballos
-   Bio: Lina ha estudiado Derecho y tiene un Máster en Ciencia Política. Ahora forma parte del equipo jurídico y político de la FSFE. Ha adquirido experiencia en la defensa de derechos digitales frente a responsables de la toma de decisiones y de las administraciones públicas. Cuenta con experiencia en asuntos legales y en el cumplimiento de licencias, así como en "community building". Contribuye a la iniciativa ¿Dinero Público? ¡Código Público!, al proyecto REUSE y a la campaña Router Freedom.

### Info personal:

-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/@lnceballosz>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/lnceballosz>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://jasyp.interferencias.tech/conducta>) durante mi participación en las jornadas
