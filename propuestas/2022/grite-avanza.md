---
layout: 2022/post
section: proposals
category: proposals
author: Rakel
title: GRITE avanza
---

# GRITE avanza

Avances del GRITE, familias por la sensibilización y formación en competencias críticas digitales. Los hitos de este año (Soberanía y Salud digital) y los objetivos para el próximo: proyecto multidisciplinar de salud digital y diagnóstico estructurado.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta (20 minutos)
-   Descripción: Presentar los avances, las nuevas relaciones interampas y a nivel regional. Visita a BCN con aFFaC y Xnet. Objetivos a desarrollar sobre el concepto salud digital, como bisagra para la sensibilización.  Necesitamos datos estructurados para fundamentar propuestas.
-   Web del proyecto:
-   Público objetivo: Ampas, familias, educadores

## Ponente:

-   Nombre: Rakel
-   Bio: Probablemente, Manu y yo. Perfiles educadores.

### Info personal:

-   Web personal: <https://tubedu.org/c/grite_gomez/videos>
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/ampagomezmoreno>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio:

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://jasyp.interferencias.tech/conducta>) durante mi participación en las jornadas
