---
layout: 2022/post
section: proposals
category: proposals
author: Jesús E.
title: Libertad, Privacidad, Seguridad e Hyperbola Project
---

# Libertad, Privacidad, Seguridad e Hyperbola Project

Hyperbola Project es un equipo de desarrolladores preocupados por la libertad, seguridad y privacidad de los usuarios/as de computadores.

Nuestro compromiso con los usuarias/os es ofrecerles un sistemas operativo totalmente libre, estable, seguro, ligero y respetando la privacidad.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga (40 minutos)
-   Descripción: Hyperbola Project es un equipo de desarrolladores preocupados por la libertad, seguridad y privacidad de los usuarios/as de computadores.

Nuestro compromiso con los usuarias/os es ofrecerles un sistemas operativo totalmente libre, estable, seguro, ligero y respetando la privacidad.

En los últimos tiempos las tecnologías se han inclinado por abusar con los datos de los usuarios/as con la ayuda de programas privativos y no solo eso sino también haciendo uso de herramientas de código abierto, nótese no confundir código abierto con software libre.

La privacidad, libertad y seguridad digital es muy difícil conseguirla, pero no imposible.
-   Web del proyecto: <https://www.hyperbola.info/>
-   Público objetivo: Usuarios/as en general, profesionales, personas interesadas en el tema.

## Ponente:

-   Nombre: Jesús E.
-   Bio: Hola, soy Jesús E. un entusiasta de tecnologías web. FrontEnd, BackEnd, DevOps e Investigador de Software, colaboro con Proyectos de Software Libre como Hyperbola Project

### Info personal:

-   Web personal: <https://jestupinanm.ga/>
-   Mastodon (u otras redes sociales libres): <https://masto.nobigtech.es/@heckyel>
-   Twitter:
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://c.hgit.ga/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://jasyp.interferencias.tech/conducta>) durante mi participación en las jornadas
