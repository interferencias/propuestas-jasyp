---
layout: 2022/post
section: proposals
category: proposals
author: Lola Martínez Pozo
title: Articulaciones entre hacking y feminismos autonomía tecnológica y autodefensa digital feminista
---

# Articulaciones entre hacking y feminismos autonomía tecnológica y autodefensa digital feminista

Los feminismos se han aproximado críticamente a las tecnologías destacando el carácter patriarcal y androcéntrico predominante, así como cuestionando las desigualdades en base al género, la sexualidad, la raza, la clase, etc., presentes en los ámbitos tecnológicos.
En las diversas aproximaciones feministas a las tecnologías podemos destacar una transición desde posturas tecnofóbicas  —caracterizadas por el rechazo a las mismas— hasta posiciones que subrayan una reapropiación crítica de las herramientas tecnológicas.
A partir de la década de los 90 con la aparición de Internet, la publicación de Manifiesto Cyborg de Donna Haraway y el desarrollo de las tecnologías digitales emergen corrientes ciberfeministas, tecnofeministas, ciber-queer, tecnoqueer y feministas decoloniales que proponen una apropiación y  transformación feminista de las tecnologías y escenarios digitales.
Más recientemente los feminismos se han articulado con la cultura hacker, open source, software y hardware libre en tanto que mecanismos y herramientas para la autonomía tecnológica feminista y la autodefensa digital feminista frente al control e invasión de las corporaciones tecnológicas y a las ciberviolencias machistas, lgtbiq-fóbicas, racistas, clasistas y diverso-fóbicas presentes en los ámbitos digitales.


## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta (20 minutos)
-   Descripción: En esta charla haremos un recorrido por las diversas aproximaciones feministas a los ámbitos tecnológicos destacando, en dicha genealogía, la mutación desde perspectivas tecnofóbicas a perspectivas que apuestan por una reapropiación y transformación feminista de las herramientas y escenarios tecnológicos.
A partir del trabajo de colectivos ciberfeministas y hackfeministas que tienen como objetivo la construcción de una Internet feminista, diversa y segura, nos centraremos en la identificación de articulaciones feministas con la cultura hacker, open source, software y hardware libre en tanto que mecanismos y estrategias para la autonomía tecnológica de los proyectos feministas frente al control, poder, vigilancia e invasión de las corporaciones tecnológicas; y como herramientas para mitigar y contrarrestar las ciberviolencias presentes en la red.

-   Web del proyecto:
-   Público objetivo: Dirigida a cualquier persona interesada en una aproximación crítica y feminista a las tecnologías digitales

## Ponente:

-   Nombre: Lola Martínez Pozo
-   Bio: Feminista, Antropóloga y Doctora en Estudios de Género. Personal Docente e Investigador de la Universidad de Granada. Investigadora en el Grupo de Investigación Social "OTRAS. Perspectivas feministas en Investigación Social", Universidad de Granada.

### Info personal:

-   Web personal: <http://wpd.ugr.es/~pfisiem/wordpress/?p=480>
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   GitLab (o cualquier sitio de código colaborativo) o portfolio:

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://jasyp.interferencias.tech/conducta>) durante mi participación en las jornadas
